; originally from https://pastebin.com/qA65Gzwg

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Don't exit at any point without user intervention
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#Persistent

; This allows loops to be interrupted in timers, when cancelling a timer
Thread, interrupt, 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; BuildUI function
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; First Label Executes?
BuildUi:
    Gui, Show, w325 h110, Jack9
    Gui, Add, Text, x10 w300, Debug:
    Gui, Add, Text, x10 w300 r5 vEdit1,
return

; Quit Script
^F12::
   Goto, CANCELALL
   Pause
   Suspend
return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
*MButton::
    SetTimer, KNIFING, 333, 100
    SetTimer, QING, 1000, 100
    KeyWait, MButton
    Goto, CANCELALL
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
QING:
    Send {q}
return

BURSTING:
    Click
    Sleep 15
return

KNIFING:
    Click
return

DANCING:
    if GetKeyState("LControl") {
        Send {LControl up}
        Sleep 15
    } else {
        Send {LControl down}
        Sleep 15
    }
return

; Default
~LButton::
    Send, {q}

;    ImageSearch, medicX, medicY, 0, 0, A_ScreenWidth, A_ScreenHeight, *120 C:\repos\PlanetsideFiles\images\MedicAura4.png
;    if (ErrorLevel < 1) {
;        GuiControl,,Edit1,Medic, found it %medicX%x%medicY%;debug
;        Send, {f}
;    } else {
;        ImageSearch, assaultX, assaultY, 0, 0, A_ScreenWidth, A_ScreenHeight, *90 C:\repos\PlanetsideFiles\images\AssaultShield1.png
;        if (ErrorLevel < 1) {
;            GuiControl,,Edit1,Assault, found it %assaultX%x%assaultY%;debug
;            Send, {f}
;        } else {
;            GuiControl,,Edit1, Found nothing ;debug
;        }
;    }


    SetTimer, QING, 1000, 100
    KeyWait, LButton
    Goto, CANCELALL
return

; DDR
LButton & XButton2::
    Send {q}
    SetTimer, BURSTING, 16, 100
    SetTimer, DANCING, 400, 100
    SetTimer, QING, 1000, 100

    KeyWait, XButton2
    KeyWait, LButton
    Goto, CANCELALL
return

; Slow Bursting
LButton & XButton1::
    Send {q}
    SetTimer, BURSTING, 75, 100
;    SetTimer, DANCING, 400, 100
;    SetTimer, QING, 1000, 100

    KeyWait, XButton1
    KeyWait, LButton
    Goto, CANCELALL
return

; Power Knife
;X::
;    Send, {n}
;    Sleep 15
;    Send, {b}
;    Sleep 15
;    KeyWait, LButton
;    Send, {b}
;return

CANCELALL:
    SetTimer, BURSTING, DELETE,
    SetTimer, DANCING, DELETE,
    SetTimer, KNIFING, DELETE,
    SetTimer, QING, DELETE,
    Send, {LControl up}
return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; prevents empty variables from being looked up as potential environment variables when PS2 is running
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#IfWinActive ahk_class Planetside2 PlayClient (Stage) x64
#NoEnv
