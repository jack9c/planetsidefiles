; originally from https://pastebin.com/qA65Gzwg

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Don't exit at any point without user intervention
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#Persistent

; This allows loops to be interrupted in timers, when cancelling a timer
Thread, interrupt, 0

global x1 := 0
global y1 := 0
global x2 := 0
global y2 := 0
global xdif := 0
global ydif := 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; BuildUI function
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; First Label Executes?
BuildUi:
    Gui, Show, w325 h110, Jack9
    Gui, Add, Text, x10 w300, Debug:
    Gui, Add, Text, x10 w300 r5 vEdit1,
return

; Quit Script
^F12::
   GuiControl,,Edit1,Suspended Script
   Goto, CANCELALL
   Pause
   Suspend
return

; Default
XButton2:: 
    Goto, mouseAutoMove
return

mouseAutoMove:
    send {LButton down}
    SetTimer mouseMove, 15
    Keywait XButton2
    Goto, CANCELALL
return

mouseMove() {
;    MouseMove A_ScreenWidth/2, A_ScreenHeight/2
MouseMove,A_ScreenWidth/2, A_ScreenHeight/2
;    DllCall("mouse_move_point","int", A_ScreenWidth/2, "int",A_ScreenHeight/2, 1)
;, "int", A_ScreenWidth/2, "int", A_ScreenHeight/2)
;    MouseGetPos x2, y2
;    xdif = x2-x1
;    ydif = y2-y1
;    MouseMove A_ScreenWidth/2, A_ScreenHeight/2
;    MouseMove 0, 200, 0, R
}


CANCELALL:
    send {LButton up}
    SetTimer, mouseMove, DELETE
return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; prevents empty variables from being looked up as potential environment variables when PS2 is running
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#IfWinActive ahk_class Planetside2 PlayClient (Stage) x64
#NoEnv
