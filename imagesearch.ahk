; originally from https://pastebin.com/qA65Gzwg

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Don't exit at any point without user intervention
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#Persistent

; This allows loops to be interrupted in timers, when cancelling a timer
Thread, interrupt, 0

global x1 := 0
global y1 := 0
global x2 := 0
global y2 := 0
global xdif := 0
global ydif := 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; BuildUI function
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; First Label Executes?
BuildUi:
    Gui, Show, w325 h110, Jack9
    Gui, Add, Text, x10 w300, Debug:
    Gui, Add, Text, x10 w300 r5 vEdit1,
return

; Quit Script
^F12::
    GuiControl,,Edit1,Suspended Script
    Pause
    Suspend
return

    
LButton::
    ImageSearch, FoundX, FoundY, 0, 0, A_ScreenWidth, A_ScreenHeight, *90 C:\repos\PlanetsideFiles\images\AssaultShield1.png
    if (ErrorLevel < 1)
        Send, {f}

    ;KeyWait, LButton
    ;if (ErrorLevel = 2)
    ;    MsgBox Could not conduct the search.
    ;else if (ErrorLevel = 1)
    ;    MsgBox Icon could not be found on the screen %A_ScreenWidth%x%A_ScreenHeight%.
    ;else
    ;    MsgBox The icon was found at %FoundX%x%FoundY%.

return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; prevents empty variables from being looked up as potential environment variables when PS2 is running
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#IfWinActive ahk_class Planetside2 PlayClient (Stage) x64
#NoEnv
