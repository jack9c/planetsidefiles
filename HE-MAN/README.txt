Used original pack, youtube, firefox video exporter, videolan, audacity+ACX Plugin

Tips: 
If the audio is too quiet, you need to add Gain in audacity, then export, then re-save sound pack. Preview again.
I think you should use The Rainbow Warrior baseline stats:
L RMS (A): 0.051756
R RMS (A): 0.051761
Samples: 44100 Hz

Achievement list used:
https://tracker.recursion.tk/?p=c&c=achievements+https://recursion.tk/showthread.php?3066-List-of-Achievements

SESSION_STARTED: Recursion Session Started
(He-Man) He-Man ^

SESSION_ENDED: Recursion Session Paused
(Orko) Cartoon bump-out

SESSION_RESUMED: Recursion Session Resumed
(Orko) Cartoon bump-in

SESSION_SAVED: Recursion Session Saved
Title sequence tail "He-Man"

PER_KILL: Kill any enemy
Random 3

HEAD_SHOT: Kill enemy infantry with a headshot
(Skeletor Laughs)

MAX_DOWN: Killing blow on a MAX
(He-Man) The bigger they are the harder they fall

HUMILIATION: Knife an Enemy
(Skeletor) Sooo, it's just you and me

TEAM_KILLER: Kill 3 teammates close together
default

PREVENT_FF: Killed 4 teammates close together
(He-MAN) Sorry Skeletor

WHOSE_SIDE: Killed 5 teammates close together
(Orko) If you can be sure of one thing...however much we love them, they return that love and more

NICE_THINGS: Teamkill with a Galaxy or Sunderer
(Horde invader) Hey, that's not supposed to happen

KILLING_SPREE: Killed 5 people without dying
(Skeletor) Let them come

DOMINATING: Killed 10 people without dying
(Skeletor) And now face that with the combined powers of The Shaping Staff and Castle Greyskull ^

UNSTOPPABLE: Killed 20 people without dying
(He-Man) What's next Skeletor? ^

WICKED_SICK: Killed 25 people without dying
(Skeletor) There is...no..way..out

GOD_LIKE: Killed 30 people without dying
(???) Why it's, it's magnificent

ONE_PLATOON: Killed 48 people without dying
(Skeletor) Sometimes my power even amazes me

ONE_MAN_EMPIRE: Killed 100 people without dying
(Skeletor) I have done it

ONE_MAN_ONE_MISSION: Killed 250 people without dying
(Skeletor) A perfect likeness of He-Man

ONE_MAN_FARM: Killed 500 people without dying
(He-Man) I became, the most powerful man in the universe

REPEAT_CUSTOMER: Killed the same enemy 3 times without dying to them
(Monty) *giggle* It's nice to see you again old friend

THINK_THEYD_LEARN: Killed the same enemy 4 times without dying to them
(He-Man) Aww he's hopeless. I suppose he'll find a way to follow me no matter what I do

DOMINATION: Killed the same enemy 5 times without dying to them
(He-Man) That's only make believe

RECURSION: Killed the same enemy 6 times without dying to them
(Skeletor) You're finished here, do you understand?

RECURSIVE_RECURSION: Killed the same enemy 10 times without dying to them
(Skeletor) And don't let me see your mangy hide around here again

CAUTIOUS_PRACTITIONER: Revived 5 teammates without dying
(Teela) I'll do whatever I can to help

MASTER_MEDIC: Revived 15 teammates without dying
(Duncan) You shouldn't have any doubts about yourself; I don't.

SHADOW_HEALER: Revived 30 teammates without dying
(Old Man) You did it. Thank you.

DO_NO_HARM: Revived 40 teammates without dying
(He-Man) I've never seen anything like it before

MAIN_HEALER: Acquired 100 combo heal XP
(He-Man) I have a hunch, that if we ever need him again *whinny* he'll be there

HEALS_REAL: Acquired 250 combo heal XP
(He-Man) Today we learned about the importance of taking the responsibility to care, about our fellow man

LIFE_FORCE: Acquired 500 combo heal XP
(He-Man) Until next time this He-Man wishing you, good health, and good luck

BASTION: Acquired 250 combo shield XP
(He-Man) And I think we're about to need you again

ENERGIZER: Acquired 500 combo shield XP
(Duncan) Guess you better be going

MEND_AND_DEFEND: Acquired 1000 combo shield XP
(Duncan) I'll be standing by if you need any help. Good luck.

PATCHWORKER: Acquired 500 combo repair XP
(He-Man) Maybe Man-at-arms can fix him up

MECHANIZED_MENDING: Acquired 1000 combo repair XP
(Duncan) What was all that about?

NANITESMITH: Acquired 2000 combo repair XP
(He-Man) I'd carry him to the other side of the planet if I had to ^

SUPPLY_DEMAND: Acquired 500 combo resupply XP
(He-Man) Anytime

ARMS_DEALER: Acquired 1000 combo resupply XP
(Adam) That's the spirit

MERCHANT_OF_DEATH: Acquired 2000 combo resupply XP
(Teela) Everyone's depending on me

COUNTER_TERRORISTS: Every 3 explosives disarmed
default
TODO: 17:20-17:25 The Royal Cousin
TODO: *boomerang sound hooking to machine* (Tri-clops) Hey!

MINE_SWEEPER: Every 3 mines destroyed
default
TODO: 16:31 The Royal Cousin
TODO: *beam sound* (He-Man) The party's over Evi-lyn.

INTERLINKED: Every 10 enemies you kill while they are on radar
(Evi-lyn) Say hello to He-Man for me.

SIDE_KICK: Get 8 assists without dying
(Adam) Without Battlecat, He-Man would be a lonely fella.

OVERWATCH: Every 3 savior kills without dying
(Duncan) I think he'll be just fine now

SUSTAINING_FORCE: Combo revived 2 teammates
(He-Man) Do whatever you can to help them out

RAPID_FIRE_REVIVAL: Combo revived 4 teammates
(Sorceress) Shouldn't you rest first? (He-Man) No time.

MIRACLE_WORKER: Combo revived 6 teammates
(He-Man) Stridor carried me here. It's only fair that I return the favor.

ZOMBIE_SUMMONER: Combo revived 8 teammates
(He-Man) One person or one living creature, can make a big difference.

ACCURACY: Killed 3 people with 3 consecutive headshots
(He-Man) You're taking a very big chance *^

IMPRESSIVE: Killed 6 people with 6 consecutive headshots
(Skeletor) Very impressive *^

SHARP_SHOOTER: Killed 9 people with 9 consecutive headshots
(Skeletor) ehhh...you're strong He-Man *^

MARKSMAN: Killed 12 people with 12 consecutive headshots
(Skeletor) You're very clever *^

HEAD_HUNTER: Killed 16 people with 16 consecutive headshots
(Skeletor) Now for a little more fun *

LOBOTOMIST: Killed 21 people with 21 consecutive headshots
(Skeletor) The might of Skeletor

KNIFE_FIGHT: Stabbed 3 people to death without dying
(Skeletor) I've had it with you're whining and bumbling

HIGH_ROLLER: Killed 2 BR100+ in a row
(Teela) You shouldn't judge books or people by the way they look

EASY_MODE: Killed 20 enemies in a row with a KDR less than 1
(Skeletor) And the same thing will happen to He-Man if he tries to stop me

HARD_MODE: Killed 7 enemies in a row with a KDR greater than 1
(He-Man) Normally I don't approve of Skeletor's methods but I have to admit, they work

NIGHTMARE: Killed 5 enemies in a row with a KDR greater than 2
(He-Man) In reality, no one can go back into the past

IMPRESS_MYSELF: Killed 3 enemies in a row with a KDR greater than 3
(Skeletor) To the palace, and my triumph

ROCKET_PRIMARY: Killed 4 people with rockets without dying
(He-Man) But we can try to learn from the past, from things that have happened to us

MASTER_JUGGLER: Killed 8 people with rockets without dying
(He-Man) Try to apply them toward being better people today

ICED: Killed 3 enemies with the NS Deep freeze
(Skeletor) Freeze them with a freeze ray

FIRE: Killed 3 enemies with a flare gun
(Skeletor) This fate is more fitting

ARMORED_ASSAULT: Killed 15 people with a vehicle
(He-Man) Remember, it's today that counts ^

BLITZKRIEG: Killed 35 people with a vehicle
(Skeletor) At last, I have the means to conquer Eternia and crush He-Man once and for all. (Mer-man) We've all heard that before Skeletor. ^

NERFED: Killed a Planetside 2 Developer
(Skeletor) So you like to play games

VIP_TEAM_KILL: Teamkill a Recursion developer
(He-Man) Am I glad to hear that, we've been through quite a lot, the two of us, I wonder what he thinks of it all

VIP_KILL: Kill a Recursion developer?
(He-Man) Now things like that are fine, but we can't always count on someone being around to protect us

REVENGE: Killed someone who has killed you multiple times in a row
(He-Man) How do you like that Hero?

PRESENT: Kill 2 people with proximity mine
(Teela) Two down and Skeletor to go.

WATCH_YOUR_STEP: Kill 4 people with proximity mine
(Orko) Now there's a neat trick

LAWNMOWER: Mowed 6 enemies with the AI Turret
(Skeletor) You can't avoid them forever

HARVESTER: Mowed 15 enemies with the AI Turret
(Duncan) As the old saying goes, if at first you don't succeed, try, try, again

DEATH_STREAK: Died 6 times in a row
(He-Man) I'd like to talk to you for just a moment about, safety

BEING_FARMED: Died 7 times in a row
(He-Man) We should practice thinking of safety all the time

DEATH_INSULT: Death streak higher than 10 deaths
(Duncan) You shouldn't have any doubts about yourself. I don't.
 
POOR_CHOICES: Revived and Died instantly twice in a row
(HE-MAN) I don't like it

MALPRACTICE: Revived and Died instantly three times in a row
(Skeletor) Curse you all

CARE_BEAR: Phase blasted 7 enemies with the Lancer
(Teela) That's some pilot

MUTUAL: Killed someone the same time they killed you
(Skeletor) I could have taken care of her myself, of course

TERRORISTS: Detonated 4 enemies with C4
(Skeletor) Skeletooorrr

SUICIDE_BOMBER: Killed someone with explosive that also killed yourself
(He-Man) You're gambling with your health, maybe even your life

WELCOME_KILL: Killed a BR1
(He-Man) Anytime you take one, your problems go away

PIZZA_DELIVERY: Killed a manned vehicle with a tank mine
(Skeletor) Here's something even stronger

ROAD_KILL: Ran over an enemy with a vehicle
(Duncan) You or someone else can get hurt

QUAD_DAMAGE: Ran over an enemy with a Flash
(He-Man intro) By the power of Grayskull. I have the Power. ^

ROAD_RAGE: Squash 4 people with a road vehicle
(Duncan) Someone could wind up losing a finger or an arm or, maybe even an eye

DEATH_FROM_ABOVE: Killed 15 ground troops with an air vehicle
(Skeletor) What kind of ship is that?

BOMBARDIER: Killed 40 ground troops with an air vehicle
(Skeletor) aaa, you leap like a swan *^

SCOURGE_OF_SKIES: Killed 60 ground troops with an air vehicle
(Royal Guard) I know a lead ship when I see one

ACE: Shot down 3 manned aircraft
(Teela) Now, that's what I call flying

TOP_GUN: Shot down 8 manned aircraft
(Teela) Nobody, but nobody can fly like that

SUPERMAN: Shot down 20 manned aircraft
(Teela) You're the best pilot I've ever seen

JUGGERNAUGHT: Tore apart 15 enemies with a Max
(He-Man) You lose Skeletor

MAXIMUM_DAMAGE: Tore apart 30 enemies with a Max
(He-Man) Let's see how well you can use that power

BOOMSTICKS: Tore apart 50 enemies with a NC Max
(Trapjaw) Runnin' won't do ya any good. Eternia is ours now, there's no place to hide.

DAKKA: Tore apart 50 enemies with a TR Max
default

ZOE_ZOE: Tore apart 50 enemies with a VS Max
default

STEEL_RAIN: Squished someone with your Drop Pod
(Orko) *hiccup* Whoops, I think I made a mistake

SHOOT_NEEDLE: Killed someone's Drop Pod
(Skeletor) C'mon let's get out of here

DECIMATION: Shot down an ESF with a Decimator
(Skeletor) Have a nice trip down.

FLYSWATTER: Shot down an anemy air craft with a MBT main gun (not Skyguard)
(He-Man) You haven't invented a trap yet, that can hold me.

EXPLOSIVE_EFFICIENCY: Killed 3 people with a frag grenade
(???) *laughing*

FRAGASM: Killed 5 people with a frag grenade
(Skeletor) Here's a little game I call...powerball.

PISTOL_WHIPPED: Killed 4 people with a hand gun without dying
(Trapjaw) You're gonna be sorry now

RUN_AND_HANDGUN: Killed 8 people with a hand gun without dying
(He-Man) I like you even less

SIDEARM_SLAYER: Killed 12 people with a hand gun without dying
(He-Man) The people who succeed are the ones who work for what they want

COMMISSIONER: Killed 3 people with the Commissioner without dying
(He-Man) Don't sell yourself short

EXECUTIONER: Executed an Infiltrator with a Commissioner
(Adam) I'm ready for seconds

EXPLODING_FIST: Punched 3 people to death in a Max without dying
(Skeletor) *laughs* If they touch you, it's going to be most unpleasant

PRECIPICE: Killed 12 people with the AV Turret without dying
(Skeletor) Who cares? *^

GUN_BIGGER: Killed 6 people with a Phalanx AV Turret without dying
(He-Man) So don't be fooled. There's always a catch to it.

BATMAN: Killed 40 people without dying using only infantry weapons
(Skeletor) What? Spoil all my fun?

CHEATER: Killed 50 people without dying using only infantry weapons
(Duncan) Being strong is fine, but there's something even better

FATALITY: Killed someone with a blunt object (like the ground)
(Skeletor) *laughing*

DOUBLE_KILL: Killed 2 people in quick succession, scoring 2 kill combo
default

TRIPLE_KILL: Killed 3 people in quick succession, scoring 3 kill combo
default

MULTI-KILL: Killed 4 people in quick succession, scoring 4 kill combo
default

MEGA_KILL: Killed 5 people in quick succession, scoring 5 kill combo
default

ULTRA_KILL: Killed 6 people in quick succession, scoring 6 kill combo
default

MONSTER_KILL: Killed 7 people in quick succession, scoring 7 kill combo
default

LUDICROUS_KILL: Killed 8 people in quick succession, scoring 8 kill combo
default

HOLY_SHIT: Killed 9 people in quick succession, scoring 9 kill combo
(He-Man) You may know some people like that; always looking for the quick way to get ahead of everybody else. Well it doesn't work that way.

RAGE_QUIT: Killed someone and they immediately quit the game
(Skeletor) I'll be back to have vengeance

SUBMISSION: Killed someone 3 times before they immediately quit the game or Submitted a support ticket about you?
(Skeletor) You'll pay for that

FIRST_BLOOD: Get a kill within the first ? seconds after starting the game ?
(He-Man) Make it the best day possible

INSTANT_ACTION: Kill an enemy within 15 seconds of logging in
(Battle Cat) Time for a fight